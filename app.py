from flask import Flask, jsonify, request, render_template
import requests
import logging
logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)

pokedex = []

@app.route('/')
def frontpage():
    return render_template('frontpage.html')

@app.route('/protected')
def protected():
    return render_template('private.html') 

@app.route('/login')
def login(): 
    return render_template('login.html')

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/signup', methods=["POST"])
def signup():
    username = request.form.get('username');
    password =  request.form.get('password');
    logging.info(f"New user: {username} {password}")
    return jsonify({'message':'user added'})

if __name__ =='__main__':  
    app.run(host='0.0.0.0', debug = True)  
